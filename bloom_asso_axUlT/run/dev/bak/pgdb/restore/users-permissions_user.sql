--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: users-permissions_user; Type: TABLE; Schema: public; Owner: aendlyx
--

CREATE TABLE public."users-permissions_user" (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    provider character varying(255),
    password character varying(255),
    "resetPasswordToken" character varying(255),
    confirmed boolean,
    blocked boolean,
    role integer
);


ALTER TABLE public."users-permissions_user" OWNER TO aendlyx;

--
-- Name: users-permissions_user_id_seq; Type: SEQUENCE; Schema: public; Owner: aendlyx
--

CREATE SEQUENCE public."users-permissions_user_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."users-permissions_user_id_seq" OWNER TO aendlyx;

--
-- Name: users-permissions_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: aendlyx
--

ALTER SEQUENCE public."users-permissions_user_id_seq" OWNED BY public."users-permissions_user".id;


--
-- Name: users-permissions_user id; Type: DEFAULT; Schema: public; Owner: aendlyx
--

ALTER TABLE ONLY public."users-permissions_user" ALTER COLUMN id SET DEFAULT nextval('public."users-permissions_user_id_seq"'::regclass);


--
-- Data for Name: users-permissions_user; Type: TABLE DATA; Schema: public; Owner: aendlyx
--

COPY public."users-permissions_user" (id, username, email, provider, password, "resetPasswordToken", confirmed, blocked, role) FROM stdin;
1	qu4Zar	pierredamiendelbreil@gmail.com	local	$2a$10$uzqbjSCZnJbL02sE5wvKX.HxAh0O0aSNIyvQqfN0lU4quV33UYfbW	\N	t	\N	1
\.


--
-- Name: users-permissions_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: aendlyx
--

SELECT pg_catalog.setval('public."users-permissions_user_id_seq"', 1, true);


--
-- Name: users-permissions_user users-permissions_user_pkey; Type: CONSTRAINT; Schema: public; Owner: aendlyx
--

ALTER TABLE ONLY public."users-permissions_user"
    ADD CONSTRAINT "users-permissions_user_pkey" PRIMARY KEY (id);


--
-- Name: search_users_permissions_user_provider; Type: INDEX; Schema: public; Owner: aendlyx
--

CREATE INDEX search_users_permissions_user_provider ON public."users-permissions_user" USING gin (provider public.gin_trgm_ops);


--
-- Name: search_users_permissions_user_resetpasswordtoken; Type: INDEX; Schema: public; Owner: aendlyx
--

CREATE INDEX search_users_permissions_user_resetpasswordtoken ON public."users-permissions_user" USING gin ("resetPasswordToken" public.gin_trgm_ops);


--
-- Name: search_users_permissions_user_username; Type: INDEX; Schema: public; Owner: aendlyx
--

CREATE INDEX search_users_permissions_user_username ON public."users-permissions_user" USING gin (username public.gin_trgm_ops);


--
-- PostgreSQL database dump complete
--

