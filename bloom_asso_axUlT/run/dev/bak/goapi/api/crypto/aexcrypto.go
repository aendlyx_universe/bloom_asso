package aexcrypto

import (
    "net/http"
    // "bytes"
    // "encoding/json"
    // "log"
    // "fmt"
    // "github.com/go-pg/pg"
    // "github.com/go-pg/pg/orm"
    "github.com/labstack/echo"
)

func AendlyxCrypto(e *echo.Echo) {
    router_aexcrypto := e.Group("aexcrypto")
    router_aexcrypto.Match([]string{"POST"}, "bitpanda", handler)
}

type AendlyxCryptoRealTimeValue struct {
    Usd float64 `json:"usd"`
    Eur float64 `json:"eur"`
    Pn int `json:"pn"`
    Pin int `json:"pin"`
    Wayperc float64 `json:"wayperc"`
}

func handler(c echo.Context) error {
    aendlyxcryptorealtimevalue := new(AendlyxCryptoRealTimeValue)
    if err := c.Bind(aendlyxcryptorealtimevalue); err != nil {
    	return err
    }
    return c.JSON(http.StatusCreated, aendlyxcryptorealtimevalue)
}
