def GenerateComp(*l,**d):

    filename = l[0]ipo
    name = l[1]
    movetofolder = l[2]
    namelow = name.lower()

    html = f"""
    div(class="{namelow}")
    """

    template = f"""
    <template lang=\042pug\042>
    {html}
    </template>

    """

    script = f"""
    <script>
    export default {{
    name: \042{name}\042,
    props: {{
    }},
    components: {{
    }},
    data: function() {{
        return {{
        }};
    }},
    created() {{
        console.log(\042{namelow}()\042)
    }},
    mounted() {{
    }},
    methods: {{
    }},
    computed: {{
    }}
    }};
    </script>

    """

    style = f"""
    <style scoped lang=\042stylus\042 >

    @media screen and (min-width 0px) and (max-width 620px)

    .{namelow}
        pass

    .{namelow} ul
        pass

    .{namelow} li
        pass

    @media screen and (min-width 620px) and (max-width 1366px)

    .{namelow}
        pass

    .{namelow} ul
        pass

    .{namelow} li
        pass

    @media screen and (min-width 1366px) and (max-width 1920px)

    .{namelow}
        pass

    .{namelow} ul
        pass

    .{namelow} li
        pass

    @media screen and (min-width 1920px)

    .{namelow}
        pass

    .{namelow} ul
        pass

    .{namelow} li
        pass

    </style >
    """
    return {
        'html': html.
        'template':template,
        'script':script,
        'style':style,
    }