def GenPackageJson(prod:dict):
  package_json=f"""
{{
  "name": "{prod['url']}",
  "version": "{prod['vsn']}",
  "private": true,
  "scripts": {{
    "serve": "vue-cli-service serve",
    "build": "vue-cli-service build"
  }},
  "dependencies": {{
    "axios": "^0.18.0",
    "core-js": "^2.6.9",
    "es6-promise": "^4.2.6",
    "moment": "^2.24.0",
    "pug": "^2.0.3",
    "pug-loader": "^2.4.0",
    "pug-plain-loader": "^1.0.0",
    "register-service-worker": "^1.5.2",
    "vue": "^2.5.21",
    "vue-carousel": "^0.18.0-alpha",
    "vue-codemirror": "^4.0.6",
    "vue-gallery": "^2.0.0",
    "vue-markdown-it": "^0.9.4",
    "vue-router": "^3.0.1",
    "vue-worker": "^1.2.1",
    "vuex": "^3.1.0"
  }},
  "devDependencies": {{
    "@vue/cli-plugin-babel": "^3.3.0",
    "@vue/cli-plugin-pwa": "^3.3.0",
    "@vue/cli-service": "^3.3.0",
    "stylus": "^0.54.5",
    "stylus-loader": "^3.0.2",
    "vue-template-compiler": "^2.5.21"
  }},
  "postcss": {{
    "plugins": {{
      "autoprefixer": {{}}
    }}
  }},
  "browserslist": [
    "> 1%",
    "last 2 versions",
    "not ie <= 8"
  ]
}}
"""
  return package_json