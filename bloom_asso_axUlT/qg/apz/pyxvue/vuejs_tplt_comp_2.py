
<template lang="pug">

div#aendlyxmain
  NavBar
  

</template>

<script>
for compname in import_components:
    str=f"import {compname}" from "./{compname.lower()}.vue"
import AendlyxMain_NavigationBar from "./navigationbar.vue"
export default {
  name: "UndergroundMain",
  props: {
  },
  components: {
    NavBar: AendlyxMain_NavigationBar
  },
  data: function() {
    return {
    };
  },
  created() {
    console.log('aendlyxmain()')
  },
  mounted() {
  },
  methods: {
  },
  computed: {
  }
};
</script> 


<style scoped lang="stylus">

@media screen and (min-width 0px) and (max-width 620px) 

  #aendlyxmain
    pass

  #aendlyxmain ul
    pass

  #aendlyxmain li
    pass

@media screen and (min-width 620px) and (max-width 1366px) 

  #aendlyxmain
    pass

  #aendlyxmain ul
    pass

  #aendlyxmain li
    pass

@media screen and (min-width 1366px) and (max-width 1920px) 

  #aendlyxmain
    pass

  #aendlyxmain ul
    pass

  #aendlyxmain li
    pass

@media screen and (min-width 1920px) 

  #aendlyxmain
    pass

  #aendlyxmain ul
    pass

  #aendlyxmain li
    pass

</style>