from pprint import pprint
import requests
import time
from datetime import datetime

# convert crypto in real business and transform them in societies assets aware and in the digital world
# collect btc prices in dollar and euro
# btc-price-usd
# btc-price-eur
# from the previous collect define if its up or down
# up
# down
# if its in the same phase as previously
# iphanum  # set the number depending on how much items are already in this phase
# set the num of phase depending on its curve (up|down)
# phanum
# calculate percentage between the previous item
# percentage

# from peewee import *
# from playhouse.postgres_ext import JSONField


# db = PostgresqlDatabase(
#     'aendlyx',
#     user='aendlyx',
#     password='9u8q3yh2f98y77832h9182yfaskjhb1298dasbd21uy',
#     host='172.23.0.5',
#     port=5432
# )


# class BaseModel(Model):
#     class Meta:
#         database = db
#         schema = 'public'


# class cryptcash_btc_bpda(BaseModel):
#     id = BigAutoField(primary_key=True)
#     up = BooleanField(default=False)
#     down = BooleanField(default=False)
#     sell_usd = DoubleField()
#     sell_eur = DoubleField()
#     buy_usd = DoubleField()
#     buy_eur = DoubleField()
#     iphanum = IntegerField()
#     phanum = IntegerField()
#     stat = DoubleField()
#     created_at = DateTimeField()
#     updated_at = DateTimeField()


# db.connect()``

previous_phase = None

bpda = {
    'dna': 'bitpanda',
    'asset': 'BTC',
    'diffper': 2,   # percentage fees
    'buy-usd': 0,   # buying price (~1,9-2%) on last check
    'sell-usd': 0,  # selling price
    'buy-eur': 0,   # buying price (~1,9-2%) on last check
    'sell-eur': 0,  # selling price
}

phase = {
    'dna': 'phase',
    'num': 0,
    'inum': 0,
    'up': False,
    'down': False,
    'percentage': 0,
}

pha = {
    'dna': 'pha',
    'phase': phase,
    'bpda': bpda,
}


def cryptcashCalculateFee(bitpanda):
    print(bitpanda)
    bitpanda['buy-usd'] = float(bitpanda['sell-usd']) + \
        float(bitpanda['sell-usd']) / 100 * 2
    bitpanda['buy-eur'] = float(bitpanda['sell-eur']) + \
        float(bitpanda['sell-eur']) / 100 * 2
    return bitpanda


def cryptcashComparePhases(previous_phase: dict, phase: dict):
    if float(previous_phase['bpda']['sell-usd']) < float(pha['bpda']['sell-usd']):
        pha['phase']['up'] = True
        pha['phase']['down'] = False
    else:
        pha['phase']['up'] = False
        pha['phase']['down'] = True
    #
    # if pha['phase']['inum'] == 0:
    #     pha['phase']['num'] += 1
    #     pha['phase']['up'] = False
    #     pha['phase']['down'] = False
    #     pha['phase']['up'] = True
    #     pha['phase']['inum'] += 1
    # else:
    #     pha['phase']['down'] = True
    #     pha['phase']['inum'] += 1


def dbxInsertPhase(phase: dict):
    # return cryptcash_btc_bpda.create(
    #     up=pha['phase']['up'],
    #     down=pha['phase']['up'],
    #     buy_usd=float(pha['bpda']['buy-usd']),
    #     buy_eur=float(pha['bpda']['buy-eur']),
    #     sell_usd=float(pha['bpda']['sell-usd']),
    #     sell_eur=float(pha['bpda']['sell-eur']),
    #     iphanum=pha['phase']['inum'],
    #     phanum=pha['phase']['num'],
    #     stat=0,
    #     created_at=datetime.now().isoformat(),
    #     updated_at=datetime.now().isoformat()
    # )
    requests.post(url="http://aendlyx.tech:1337/bitpandas", json={"jsdt":dict(
        up=pha['phase']['up'],
        down=pha['phase']['up'],
        buy_usd=float(pha['bpda']['buy-usd']),
        buy_eur=float(pha['bpda']['buy-eur']),
        sell_usd=float(pha['bpda']['sell-usd']),
        sell_eur=float(pha['bpda']['sell-eur']),
        iphanum=pha['phase']['inum'],
        phanum=pha['phase']['num'],
        stat=0,
        created_at=datetime.now().isoformat(),
    )})
    



while True:
    if not previous_phase:
        previous_phase = pha
    else:
        resp = requests.get("https://api.bitpanda.com/v1/ticker")
        prices_rjs = resp.json()

        bpda = {
            'dna': 'bitpanda',
            'asset': 'BTC',
            'diffper': 2,
            'sell-usd': prices_rjs['BTC']['USD'],
            'sell-eur': prices_rjs['BTC']['EUR'],
        }
        bpda = cryptcashCalculateFee(bpda)

        phase = {
            'dna': 'phase',
            'num': 0,
            'inum': 0,
            'up': False,
            'down': False,
            'percentage': 0,
        }

        pha = {
            'dna': 'pha',
            'phase': phase,
            'bpda': bpda,
        }
        cryptcashComparePhases(previous_phase, pha)
        try:
            previous_phase = pha
            resp = dbxInsertPhase(pha)
        except Exception as exp:
            print(exp)
        finally:
            time.sleep(50)
