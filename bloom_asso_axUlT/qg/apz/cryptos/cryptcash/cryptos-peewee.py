# coding:utf-8

from peewee import *

from datetime import datetime

db = PostgresqlDatabase(
	'twist',
	user='twist',
	password='kl;qwe',
	host='127.0.0.1',
	port=5432
)

class BaseModel(Model):
	class Meta:
		database = db
		schema = 'paris'

class Promoter(BaseModel):
	id_promoter = BigAutoField(primary_key=True)
	name = CharField(unique=True)

class Event(BaseModel):
	id_event = BigAutoField(primary_key=True)
	fk_promoter = ForeignKeyField(Promoter, backref='events')
	description = TextField()
	start_dt = 
	fin_dt =

class List(BaseModel):
	id_list = BigAutoField(primary_key=True)
	fk_event = ForeignKeyField(Event, backref='lists')

class Slot(BaseModel):
	id_slot = BigAutoField(primary_key=True)
	fk_list = ForeignKeyField(List, backref='slots')

db.connect()
db.create_tables([Promoter, Event, List, Slot])

rex = Promoter.create(name='rex')

event_rex = Event.create(
	fk_promoter=rex,
	description="Lourd Event"
)

list_event_rex = List.create(
	fk_event=event_rex
)

slot_list_event_rex = Slot.create(
	fk_list=list_event_rex
)

for event in rex.events:
	print(event)

# __all__ = [
# 	'db',
# 	'BaseModel',
# 	'Promoter',
# 	'Event',
# ]