
import requests
import csv
import datetime
import pprint

resp = requests.get(
    'http://192.168.43.52:1337/cryptorealtimestamps?_limit=1000000')

rjs = resp.json()

phases = {
    'atom': [],
    'bch': [],
    'btc': [],
    'dash': [],
    'trx': [],
    'xrp': [],
    'xtz': []
}


def compare_price_with_before(before, now, key):
    # print(f'{before}{now}{key}')
    if before and now and key:
        if now[key] > before[key]:
            print(f'{before[key]} < {now[key]}')
            return now[key]
        else:
            return None


for cnt, dk in enumerate(rjs):
    for k, v in dk.items():
        if cnt > 0:
            for _k in phases.keys():
                # print(cnt)
                # print(k)
                # print(v)
                # print(_k)
                # print()
                res = compare_price_with_before(
                    rjs[cnt-1],
                    rjs[cnt],
                    _k+"-price"
                )
                if res:
                    print(_k)
                    print(phases[_k])
                    print(res)
                    # phases[_k].append(res)
# pprint.pprint(phases)
