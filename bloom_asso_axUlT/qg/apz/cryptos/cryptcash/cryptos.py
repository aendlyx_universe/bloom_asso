# importing libraries
import requests
import datetime
import time
import subprocess

# static_data
# api routes by assets
api = {
    'btc':   'https://api.cryptowat.ch/markets/bitfinex/btcusd/price',
    'dash':   'https://api.cryptowat.ch/markets/bitfinex/dashusd/price',
    'xrp':   'https://api.cryptowat.ch/markets/bitfinex/xrpusd/price',
    'bch':   'https://api.cryptowat.ch/markets/bitfinex/bchusd/price',
    'xtz':   'https://api.cryptowat.ch/markets/bitfinex/xtzusd/price',
    'trx':   'https://api.cryptowat.ch/markets/bitfinex/trxusd/price',
    'atom':   'https://api.cryptowat.ch/markets/gateio/atomusdt/price',
}
# define stock levels
cryptos = {
    'btc':   0.3766,
    'dash':  0.3438,
    'xrp':   125.5212,
    'bch':   0.1598,
    'xtz':   66,
    'trx':   2154.269368,
    'atom':  10.745,
}
# functions for routine


def respExtractPrices(api, prices):
    for k, v in api.items():
        prices[k] = requests.get(v).json()['result']['price']
    return prices

# start routine


while True:

    # extract prices
    prices = respExtractPrices(api, {})

    # calculate capital

    for k, v in prices.items():

        _capital = v*cryptos[k]
        cryptos[k] = {
            'capital':   _capital,
            'stock':   cryptos[k],
            'price': v
        }
        del _capital

    total = 0

    for v in cryptos.values():
        total = total+v['capital']

    export = {
        'a-date-time':   datetime.datetime.now().isoformat(),
        'cryptos':   cryptos,
        'total':   total,
    }

    c = export['cryptos']

    cryptorealtimestamp = {
        'timestamp': export['a-date-time'],
        'atom-capital': c['atom']['capital'],
        'atom-price': c['atom']['price'],
        'atom-stock': c['atom']['stock'],
        'bch-capital': c['bch']['capital'],
        'bch-price': c['bch']['price'],
        'bch-stock': c['bch']['stock'],
        'btc-capital': c['btc']['capital'],
        'btc-price': c['btc']['price'],
        'btc-stock': c['btc']['stock'],
        'dash-capital': c['dash']['capital'],
        'dash-price': c['dash']['price'],
        'dash-stock': c['dash']['stock'],
        'trx-capital': c['trx']['capital'],
        'trx-price': c['trx']['price'],
        'trx-stock': c['trx']['stock'],
        'xrp-capital': c['xrp']['capital'],
        'xrp-price': c['xrp']['price'],
        'xrp-stock': c['xrp']['stock'],
        'xtz-capital': c['xtz']['capital'],
        'xtz-price': c['xtz']['price'],
        'xtz-stock': c['xtz']['stock'],
        'total': export['total'],
        'totalE': total*0.89,
    }

    resp = requests.post(
        'http://aex-spi:1337/cryptorealtimestamps', json=cryptorealtimestamp)

    cryptorealtimetotal = {
        'timestamp': export['a-date-time'],
        'total': export['total'],
        'cryptorealtimestamp': resp.json()['id']
    }

    resp = requests.post(
        'http://aex-spi:1337/cryptorealtimetotals', json=cryptorealtimetotal)

    time.sleep(20)
    # add indicator from the last period to set if it goes up or down
    # add percentage of differences that would be helpfull for bot
