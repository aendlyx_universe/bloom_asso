
import requests
import csv
import datetime

resp = requests.get(
    'http://192.168.1.99:1337/cryptorealtimestamps?_limit=1000000')
rjs = resp.json()
resp_len = len(rjs)
print(f'resp_len : {resp_len}')
# print(f'{rjs[0].keys()}')
csv_columns = []
for c in rjs[0].keys():
    csv_columns.append(c)
print(csv_columns)
now = datetime.datetime.now().isoformat()
csv_file = f'cryptcash_{now}.csv'
try:
    with open(csv_file, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
        writer.writeheader()
        for data in rjs:
            writer.writerow(data)
except IOError:
    print("I/O error")
