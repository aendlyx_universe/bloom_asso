
# ---------------
# GENERATE API
# ---------------

todo="""
    create folders
    create files
    add local mod to import
    create mod 
"""

#[1]
# package main
# import (
# 	"net/http"
# 	"github.com/labstack/echo"
# 	"github.com/labstack/echo/middleware"

#[2]
# 	"./strapi"
# 	"./crypto"
# )

#[3]
# func main() {
# 	e := echo.New()
# 	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
# 		AllowOrigins: []string{"http://localhost:8080"},
# 		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
# 	}))
# 	// e.POST("/users", addUser)

# 	e.Use(middleware.Logger())
# 	e.Use(middleware.Recover())
# 	e.Use(middleware.Secure())

#[4]
# 	aexstrapi.New(e)
# 	aexcrypto.AendlyxCrypto(e)

#[5]
# 	e.Logger.Fatal(e.Start(":10000"))
# }


# ---------------
# GENERATE MODULE
# ---------------

mdl={
    #package-name
    'pkgn':'aexcrypto',
    #import-stdlib
    'istd':[
        "net/http",
        "bytes",
        "encoding/json",
        "log",
        "fmt"
    ],
    #import-git
    'igit':[
        "github.com/go-pg/pg",
        "github.com/go-pg/pg/orm",
        "github.com/labstack/echo",
    ],
    #main-function-name-capitalized
    'mfun':'AendlyxCrypto',
    #groupe-route-name
    'grpn':'aexcrypto',
    #verbs-name
    'vrbn':'POST',
    #subroute-name
    'srtn':'bitpanda',
    #handler
    'hdlr':'handler',
    #struct
    'strn':'AendlyxCryptoRealTimeValue',
    #struct-fields
    'strf':dict(
        usd="float64",
        eur="float64",
        pn="int",
        pin="int",
        wayperc="float64"
    )
}

pkgn=f"package { mdl['pkgn'] }\n"
print(pkgn)

impt=f"""import (\n"""
for i in mdl['istd']:
    impt+=f"""    "{ i }"\n"""
for i in mdl['igit']:
    impt+=f"""    "{ i }"\n"""
impt+=")"
print(impt)

mfun=f"""
func { mdl['mfun'] }(e *echo.Echo) {{
    router_{ mdl['pkgn'] } := e.Group("{ mdl['grpn'] }")
    router_{mdl['pkgn']}.Match([]string{{"{ mdl['vrbn'] }"}}, "{ mdl['srtn'] }", { mdl['hdlr'] })
}}
"""
print(mfun)

struct=f"""type { mdl['strn'] } struct {{\n"""
for key , val in mdl['strf'].items():
    Ckey=key.capitalize()
    struct+=f"""    {Ckey} {val} `json:"{key}"`\n"""
struct+=f"""}}"""
print(struct)

hdlr=f"""func { mdl['hdlr'] }(c echo.Context) error {{
    { mdl['strn'].lower() } := new({mdl['strn']})
    if err := c.Bind({ mdl['strn'].lower() }); err != nil {{
    	return err
    }}
    return c.JSON(http.StatusCreated, { mdl['strn'].lower() })
}}
"""
print(hdlr)