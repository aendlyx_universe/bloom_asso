freelance = {
    'fnam': null,
    'lnam': null,
    'age': null,
    'job': null}

offer = {
    'name': null,
    'tasks': [],
    'company': null,
    'location': null}

contact = {
    'origin': null,
    'email': null,
    'phone': null}