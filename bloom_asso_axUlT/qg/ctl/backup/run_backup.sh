#!/bin/sh

BACKUP_VERSION=`find ./dbz/* -maxdepth 0 -type d | wc -l`
mkdir -p ./dbz/$BACKUP_VERSION/
cd ./dbz/$BACKUP_VERSION/

docker cp -a aex-spi:/usr/aendlyxstrapi/api .
docker cp -aL aex-spi-pg:/var/lib/postgresql/aexpgdb .
docker exec -it aex-spi-pg pg_dump -U aendlyx -d aendlyx -t "public.users-permissions_user" > users-permissions_user.sql

zip -r pgdb.zip aexpgdb
rm -rf aexpgdb
zip -r sapi.zip api
rm -rf api

#ssha
#git add --all
#git commit -am "new backup version : ${BACKUP_VERSION}"
#git push
