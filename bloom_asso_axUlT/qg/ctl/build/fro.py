import shutil
import subprocess
import os

project=os.getcwd().split('/')[-1]

dists='../dists/'+project

if os.path.exists('node_modules'):
    pass
else:
    subprocess.run('npm install', shell=True)

subprocess.run('npm run build', shell=True)
shutil.copytree('./dist', dists)
