import os
import shutil
import subprocess
import json
import uuid
from hashlib import blake2b
import yaml

# create tmp directory where we are gonna generate the new aendlyx UlT stack
try:
  print('`> CREATING_FILES_AND_FOLDERS')
  tpltdirname='{{cookiecutter.project_name}}'
  tpltcnffile='cookiecutter.json'
  os.makedirs(tpltdirname)
except Exception as exp:
  raise exp

# pre-generate passwords
passwords=[
  blake2b(bytes(str(uuid.uuid1()).encode('utf-8'))).hexdigest(),
  blake2b(bytes(str(uuid.uuid1()).encode('utf-8'))).hexdigest(),
  blake2b(bytes(str(uuid.uuid1()).encode('utf-8'))).hexdigest(),
  str(uuid.uuid1()).replace('-', ''),
]

# create cookiecutter.json
cli={
  'project_name': input('`> project_name :'),
  'url_production': input('`> url_production :'),
  'email_admin': input('`> email_admin :')
}
cli = { **cli, **{
  'url_git_repo': input('`> url_git_repo :'),
  'git_repo_branch': input('`> git_repo_branch :')
  }
}
cli = { **cli, **{
  'dbname': f"axUlT_{cli['project_name']}",
  'dbusr': input('`> dbuser :'),
  'dbpwd': passwords.pop(),
  'ax_ult_pga_pwd': passwords.pop(),
  'strapi_secret_key_1': passwords.pop(),
  'strapi_secret_key_2': passwords.pop(),
  }
}
cli = { **cli, **{
  'network_name': cli['project_name'].replace('.', '_')
  }
}

json.dump(cli, open(tpltcnffile, 'w'))

subprocess.run('ls -la', shell=True)
subprocess.run(f'cat {tpltcnffile}', shell=True)
subprocess.run(f'cp -rfv run {tpltdirname}', shell=True)
subprocess.run(f'mkdir {tpltdirname}/env ; cp -rfv env/master.yml {tpltdirname}/env/ ', shell=True)
subprocess.run(f'cookiecutter .', shell=True)
subprocess.run(f"cp -rfv qg {cli['project_name']}", shell=True)
#subprocess.run(f'cp -rfv env/pyx {tpltdirname}/env/ ; cp -rfv env/zsh {tpltdirname}/env/', shell=True)
print(os.getcwd())
subprocess.run('ls -la', shell=True)
os.chdir(cli['project_name'])


master_env = yaml.load(open('./env/master.yml', 'r'), Loader=yaml.FullLoader)
global_env = master_env['AENDLYX_ULT']['GLOBAL']
dev_env = master_env['AENDLYX_ULT']['DEVELOP_ENV']
docker_env_file = open('./env/.env', 'a')
docker_env_file.write('\n# GLOBAL\n')
for k,v in global_env.items():
  docker_env_file.write(f'{k}={v}\n')
docker_env_file.write('\n# DEVELOP_ENV\n')
for k,v in dev_env.items():
  docker_env_file.write(f'\n# {k}\n')
  for k2,v2 in v.items():
    docker_env_file.write(f'{k2}={v2}\n')
docker_env_file.close()

os.chdir('..')

#: TODO
# make fro dists symlinks

# remove temp file
shutil.rmtree(tpltdirname)
os.remove(tpltcnffile)
shutil.move(cli['project_name'], f"/opt/{cli['project_name']}")

# make env symlinks
subprocess.run(f"ln -s /opt/{cli['project_name']}/env/.env /opt/{cli['project_name']}/run/dev/bak/cms/.env", shell=True)
subprocess.run(f"ln -s /opt/{cli['project_name']}/env/.env /opt/{cli['project_name']}/run/dev/bak/goapi/.env", shell=True)
subprocess.run(f"ln -s /opt/{cli['project_name']}/env/.env /opt/{cli['project_name']}/run/dev/bak/pgdb/.env", shell=True)
subprocess.run(f"ln -s /opt/{cli['project_name']}/env/.env /opt/{cli['project_name']}/run/dev/bak/proxy/.env", shell=True)
